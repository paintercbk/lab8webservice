package th.ac.tu.siit.lab8webservice;
import java.io.*;
import java.net.*;
import java.util.*;
import org.json.*;
import android.os.*;
import android.app.*;
import android.net.*;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.*;

public class MainActivity extends ListActivity {
	List<Map<String,String>> list;
	SimpleAdapter adapter;
	Long lastUpdate = 0l;
	String location= "bangkok";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		//Do not allow the user to change the orientation of the application
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_NOSENSOR);
		list = new ArrayList<Map<String,String>>();
		adapter = new SimpleAdapter(this, list, R.layout.item, 
				new String[] {"name", "value"}, 
				new int[] {R.id.tvName, R.id.tvValue});
		setListAdapter(adapter);
		
		File infile = getBaseContext().getFileStreamPath("location.tsv");
		if(infile.exists())
		{
			try {
				Scanner sc = new Scanner(infile);
				while(sc.hasNextLine()) {
					String line = sc.nextLine();
					location = line;
					
				}
				sc.close();
			} catch (FileNotFoundException e) {
				//Do nothing
			}
		}
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		refresh();
	}
	
	public void refresh()
		{
		//Check if the device has Internet connection
				ConnectivityManager mgr = (ConnectivityManager)
						getSystemService(Context.CONNECTIVITY_SERVICE);
				NetworkInfo info = mgr.getActiveNetworkInfo();
				if (info != null && info.isConnected()) {
					//load data when there is a connection
					long current = System.currentTimeMillis();
					if (current - lastUpdate > 3*60*1000) {
						WeatherTask task = new WeatherTask(this);
						task.execute("http://cholwich.org/bangkok.json");
					}
				}
				else {
					Toast t = Toast.makeText(this, 
							"No Internet Connectivity on this device. Check your setting", 
							Toast.LENGTH_LONG);
					t.show();
				}
		}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item)
	{
		WeatherTask task = new WeatherTask(this);
		switch(item.getItemId())
		{
		case R.id.non:	
			try {
				FileOutputStream outfile = openFileOutput("location.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write("nonthaburi");
				p.flush(); p.close();
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			location="nonthaburi";
			task.execute("http://cholwich.org/nonthaburi.json");		
			return true;			
		case R.id.bkk:
			try {
				FileOutputStream outfile = openFileOutput("location.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write("bangkok");
				p.flush(); p.close();
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			location="bangkok";
			task.execute("http://cholwich.org/bangkok.json");
			return true;
		case R.id.pttn:
			try {
				FileOutputStream outfile = openFileOutput("location.tsv", MODE_PRIVATE);
				PrintWriter p = new PrintWriter(outfile);
				p.write("pathumthani");
				p.flush(); p.close();
			} catch (Exception e) {
				Toast t = Toast.makeText(this, "Error: Unable to save data", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			location="pathumthani";
			task.execute("http://cholwich.org/pathumthani.json");
			return true;
		case R.id.refresh:
				/*Intent i = new Intent(); 
				startActivityForResult(i, 9999);
				return true;*/
			
			long current = System.currentTimeMillis();
			
			if (current - lastUpdate > 3*10*1000) {
				String locURL = "http://cholwich.org/"+location+".json";
				
				task.execute(locURL);
			}else{
				Toast t = Toast.makeText(this, "Can't refresh, it's just "
			+(current-lastUpdate)/1000+" sec from last refreshed", 
						Toast.LENGTH_SHORT);
				t.show();
			}
			return true;

		}			
		return false;
		
	}

	//sub class
	class WeatherTask extends AsyncTask<String, Void, String> {
		Map<String,String> record;
		ProgressDialog dialog;
		
		public WeatherTask(MainActivity m) {
			dialog = new ProgressDialog(m);
		}
		
		public WeatherTask(WeatherTask weatherTask) {
			// TODO Auto-generated constructor stub
		}

		//Execute in UI thread.
		@Override
		protected void onPreExecute() { //call  b/f execute time-consuming
			super.onPreExecute();
			dialog.setMessage("Loading Weather Data");
			dialog.show();
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			if (dialog.isShowing()) {
				dialog.dismiss();
			}
			Toast t = Toast.makeText(getApplicationContext(), 
					result, Toast.LENGTH_LONG);
			t.show();
			adapter.notifyDataSetChanged();
			lastUpdate = System.currentTimeMillis(); //if user come back to application again in 5 min, this application wont load thw new one
			setTitle(location+" Weather");
		}
		
		//Executed in BG thread
		@Override
		protected String doInBackground(String... params) {
			BufferedReader in = null;
			StringBuilder buffer = new StringBuilder();
			String line;
			int response;
			try {
				
				//Get the first parameter, set is as a URL
				URL url = new URL(params[0]);
				//Create a connection to the URL
				HttpURLConnection http = (HttpURLConnection)url.openConnection();
				http.setReadTimeout(10000);
				http.setConnectTimeout(15000);
				http.setRequestMethod("GET");
				
				//We want to download data from the URL
				http.setDoInput(true);
				http.connect();
				
				response = http.getResponseCode();
				if (response == 200) {
					in = new BufferedReader(new InputStreamReader(http.getInputStream()));
					while((line = in.readLine()) != null) {
						buffer.append(line);
					}
					JSONObject json = new JSONObject(buffer.toString());
					JSONObject jmain = json.getJSONObject("main");
					JSONObject jwind = json.getJSONObject("wind");
					list.clear();
					record = new HashMap<String,String>();
					record.put("name", "Temperature");
					double temp = jmain.getDouble("temp")-273.0;
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp));
					list.add(record);
					//Add description
					//weather:[{"description": "broken...", }],
					JSONArray jweather = json.getJSONArray("weather");
					JSONObject w0 = jweather.getJSONObject(0);
					String description = w0.getString("description");
					record = new HashMap<String,String>();
					record.put("name", "Description");
					record.put("value", description);
					list.add(record);					
					
					String pressure = jmain.getString("pressure");
					record = new HashMap<String,String>();
					record.put("name", "Pressure");
					record.put("value", pressure+" hPa");
					list.add(record);
					
					String humidity = jmain.getString("humidity");
					record = new HashMap<String,String>();
					record.put("name", "Humidity");
					record.put("value", humidity+" %");
					list.add(record);
					
					String temp_min = jmain.getString("temp_min");
					double temp_minc = jmain.getDouble("temp_min")-273.0;
					record = new HashMap<String,String>();
					record.put("name", "Min Temperature");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp_minc));
					list.add(record);
					
					String temp_max = jmain.getString("temp_max");
					double temp_maxc = jmain.getDouble("temp_max")-273.0;
					record = new HashMap<String,String>();
					record.put("name", "Max Temperature");
					record.put("value", String.format(Locale.getDefault(), "%.1f degree celsius", temp_maxc));
					list.add(record);
					
					String speed = jwind.getString("speed");
					record = new HashMap<String,String>();
					record.put("name", "Wind Speed");
					record.put("value", speed+" mps");
					list.add(record);
					
					double  deg = jwind.getDouble("deg");
					record = new HashMap<String,String>();
					record.put("name", "Wind Degree");
					record.put("value", deg+" degree");
					list.add(record);
					
					
					return "Finished Loading Weather Data";
				}
				else {
					return "Error "+response;
				}
			} catch (IOException e) {
				return "Error while reading data from the server";
			} catch (JSONException e) {
				return "Error while processing the downloaded data";
			}	
		}
	}
}

